FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y git software-properties-common ssh && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN add-apt-repository -y ppa:rmescandon/yq && \
    apt-get update && \
    apt-get install -y yq && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*